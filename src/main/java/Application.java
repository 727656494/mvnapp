import java.util.UUID;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mvnapp.test.entity.PersonEntity;
import com.mvnapp.test.util.HibernateUtil;

public class Application {

	public static void main(String[] args) {
		Session session = HibernateUtil.getSession();
		Transaction tx = session.beginTransaction();
		
		PersonEntity p1 = new PersonEntity();
		p1.setId(UUID.randomUUID().toString());
		p1.setLogin("mja2");
		p1.setName("jianMing");
		p1.setPassword("123456");
		p1.setRole("ROLE_USER");
		
		PersonEntity p2 = new PersonEntity();
		p2.setId(UUID.randomUUID().toString());
		p2.setLogin("mja3");
		p2.setName("jianMing");
		p2.setPassword("123456");
		p2.setRole("ROLE_USER");
		
		session.save(p1);
		session.save(p2);
		
		tx.commit();
		HibernateUtil.closeSession();
	}

}
