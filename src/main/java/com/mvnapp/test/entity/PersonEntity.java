package com.mvnapp.test.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Entity
@Data
@Table(name = "t_person")
public class PersonEntity implements java.io.Serializable {
	private static final long serialVersionUID = -4376187124011546736L;
	@Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String id;
    //姓名昵称
    @Column(columnDefinition = "VARCHAR(25) COMMENT '姓名(昵称)'")
    private String name;
    //帐号（统一手机号作为帐号）
    @Column(unique=true, columnDefinition = "VARCHAR(25) NOT NULL COMMENT '帐号'")
    private String login;
    //密码
    @Column(columnDefinition = "VARCHAR(50) COMMENT '密码'")
    private String password;
    //角色身份（用户,工程师,管理员,企业）
	@Column(columnDefinition = "VARCHAR(25) NOT NULL COMMENT '角色'")
	private String role;
}

